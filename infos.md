## Orientation première

Les vœux pour la première sont à effectuer avant le **21 Mai**.

## Certificats de scolarité

Les certificats de scolarité sont disponibles sur Pronote. Ils ne seront pas
imprimés.

---

## Réunion parents-professeurs

La réunion parents-professeurs a eu lieu le **samedi 23 Septembre**, le matin.

Voici **le [diaporama](pdfs/parents-s11.pdf)** utilisé pendant la réunion.
