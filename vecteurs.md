## Vecteurs

### Fichiers de cours

*Pour accéder aux fichiers, ouvrez les dans un nouvel onglet.*

+ [Cours](https://erou.forge.apps.education.fr/s11/pdfs/cours-vecteurs.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/s11/pdfs/exos-vecteurs.pdf)

---

### D'autres ressources

Comme pour les autres chapitres, Yvan Monka a réalisé une *playlist* au sujet
des vecteurs.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=SUnkfOX5RlEfrjdp&amp;list=PLVUDmbpupCaoKRWtIqAp-FOFSQwftAOtA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

À venir.
