## Orientation

### CIO

Le CIO (Centre d'Information et d'Orientation) se trouve au 18 rue
Charles Rossignol (91 600 Savigny sur Orge), juste à côté du lycée
Gaspard Monge.

Plus de détails sur [leur site Internet](https://www.ac-versailles.fr/cio-savigny).

### Sur Internet

- Le site [Horizons 21e](https://www.horizons21.fr/) permet de simuler des choix
  de spécialité pour découvrir quelles perspectives de formations et de métiers
  sont en adéquation.
- Le site de l'[ONISEP](https://www.onisep.fr/) est dédié à l'orientation,
  l'ONISEP (Office national d'information sur les enseignements et les
  professions) a pour vocation d'offrir toutes les informations sur les études
  et les métiers.
