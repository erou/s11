## Résolution d'équation

### Fichiers de cours

*Pour accéder aux fichiers, ouvrez les dans un nouvel onglet.*

+ [Cours](https://erou.forge.apps.education.fr/s11/pdfs/cours-equations.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/s11/pdfs/exos-equations.pdf)

### Des resssources en ligne

Yvan Monka vous rappelle comment résoudre une équation sur sa chaîne.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/6gW4rXWr3fY?si=fvwbeWA1PEoa0ZNS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

### Pour aller plus loin

Nous résolvons dans ce chapitre des équations du premier dégré, mais peut-on
faire plus difficile ? Et le deuxième degré alors ? C'est au programme de la
spécialité en première générale, mais on en parle un peu aussi dans cette vidéo. 

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/M6o5CRYfNxA?si=0MDgThIhq-DR9LEI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
