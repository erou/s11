## Probabilités

### Fichiers de cours

*Pour accéder aux fichiers, ouvrez les dans un nouvel onglet.*

+ [Cours](https://erou.forge.apps.education.fr/s11/pdfs/cours-probas.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/s11/pdfs/exos-probas.pdf)

---

### D'autres ressources

Si vous avez suivi les chapitres précédents, vous vous doutez du lien qui va
suivre...

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=UipC0i6UC084U3_C&amp;list=PLVUDmbpupCaoTbm5IPhDmnM7qnETXp47B" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

À venir.
