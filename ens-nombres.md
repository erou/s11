## Ensembles de nombres

### Fichiers de cours

*Pour accéder aux fichiers, ouvrez les dans un nouvel onglet.*

+ [Cours](https://erou.forge.apps.education.fr/s11/pdfs/cours-ens-nombres.pdf)
+ [Exercices](https://erou.apps.education.fr/s11/pdfs/exos-ens-nombres.pdf)

---

### Pour aller plus loin

Une vidéo sur l'ensemble des nombres qui ne sont pas rationnels, que l'on
appelle *nombres irrationnels* et que l'on peut noter
$$\mathbb{R}\setminus\mathbb{Q}$$.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/3PK2Wm7_HSI?si=TMfxOi9VfGizKDDM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
