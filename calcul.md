## Calcul littéral

### Fichiers de cours

*Pour accéder aux fichiers, ouvrez les dans un nouvel onglet.*

+ [Cours](https://erou.forge.apps.education.fr/s11/pdfs/cours-calcul.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/s11/pdfs/exos-calcul.pdf)

---

### Quelques ressources

Une *playlist* d'Yvan Monka concernant le calcul littéral : c'est très complet,
il y a sûrement ce que vous cherchez.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=BMzKdsVUOUZFt5f5&amp;list=PLVUDmbpupCardIQnUIk2NsPvqKlyBU8jN" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
