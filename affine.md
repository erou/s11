## Fonctions affines

### Fichiers de cours

*Pour accéder aux fichiers, ouvrez les dans un nouvel onglet.*

+ [Cours](https://erou.forge.apps.education.fr/s11/pdfs/cours-fonctions-affines.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/s11/pdfs/exos-fonctions-affines.pdf)

---

### D'autres ressources

Comme le cours est relativement rapide, pas de longue playlist cette fois-ci,
mais toujours le précieux travail d'Yvan Monka !

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/n5_pRx4ozIg?si=7-ILS4HCQwcAJtfj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
