## Généralités sur les fonctions

### Fichiers de cours

*Pour accéder aux fichiers, ouvrez les dans un nouvel onglet.*

+ [Cours](https://erou.forge.apps.education.fr/s11/pdfs/cours-generalites-fonctions.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/s11/pdfs/exos-generalites-fonctions.pdf)

---

### D'autres ressources

Pour réviser rien de tel que la compagnie de Monsieur Yvan Monka.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=ZdRQW8MEYLHt7qfF&amp;list=PLVUDmbpupCaomVd5RgDPWGMbH0voQeRdZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

À venir.
