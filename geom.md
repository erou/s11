## Géométrie dans le plan

### Fichiers de cours

*Pour accéder aux fichiers, ouvrez les dans un nouvel onglet.*

+ [Cours](https://erou.forge.apps.education.fr/s11/pdfs/cours-géométrie-plan.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/s11/pdfs/exos-géométrie-plan.pdf)

---

### D'autres ressources

Pour se rappeler du théorème de Pythagore !

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/BeHlrhlAPbg?si=G-vPITd88sQMRiMK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

Pour passer de la géométrie du plan à la géométrie dans l'espace, et au-delà !

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Ufx9x-JzZjQ?si=VOsLfpkFktlbnIUT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
