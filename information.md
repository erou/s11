## Information chiffrée

### Fichiers de cours

*Pour accéder aux fichiers, ouvrez les dans un nouvel onglet.*

+ [Activité d'introduction](https://erou.forge.apps.education.fr/s11/pdfs/activite-information-chiffree.pdf)
+ [Cours](https://erou.forge.apps.education.fr/s11/pdfs/cours-information-chiffree.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/s11/pdfs/exos-information-chiffree.pdf)

---

### D'autres ressources

Encore une *playlist* de monsieur Yvan Monka, très complète.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=gIfsfzrqqzf6nUVK&amp;list=PLVUDmbpupCapnOWqCISVKTwjAcUhfw_Lt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

Une réflexion sur la mesure des nombres, que l'on pourra rapprocher des notions
de variations absolue et relative.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/wS1Tsj_fl5o?si=zA3enZa8nZO_Paw_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
